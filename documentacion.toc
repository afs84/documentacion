\select@language {spanish}
\contentsline {chapter}{\'{I}ndice general}{\es@scroman {ii}}
\contentsline {chapter}{\IeC {\'I}ndice de Figuras}{\es@scroman {iv}}
\contentsline {chapter}{\IeC {\'I}ndice de Tablas}{\es@scroman {viii}}
\contentsline {chapter}{\numberline {1}Introducci\IeC {\'o}n}{1}
\contentsline {section}{\numberline {1.1}Descripci\IeC {\'o}n general}{1}
\contentsline {section}{\numberline {1.2}Motivaci\IeC {\'o}n}{2}
\contentsline {section}{\numberline {1.3}Otras gu\IeC {\'\i }as virtuales}{2}
\contentsline {section}{\numberline {1.4}Glosario}{3}
\contentsline {subsection}{\numberline {1.4.1}T\IeC {\'e}rminos}{3}
\contentsline {section}{\numberline {1.5}Herramientas utilizadas}{3}
\contentsline {section}{\numberline {1.6}Organizaci\IeC {\'o}n de la documentaci\IeC {\'o}n }{5}
\contentsline {chapter}{\numberline {2}Planificaci\IeC {\'o}n }{7}
\contentsline {section}{\numberline {2.1}Metodolog\IeC {\'\i }a de desarrollo}{7}
\contentsline {subsection}{\numberline {2.1.1}Iteraci\IeC {\'o}n 1:}{7}
\contentsline {subsubsection}{\numberline {2.1.1.1}Iteraci\IeC {\'o}n 2:}{7}
\contentsline {subsubsection}{\numberline {2.1.1.2}Iteraci\IeC {\'o}n 3:}{8}
\contentsline {subsubsection}{\numberline {2.1.1.3}Iteraci\IeC {\'o}n 4:}{8}
\contentsline {subsubsection}{\numberline {2.1.1.4}Iteraci\IeC {\'o}n 5:}{8}
\contentsline {subsubsection}{\numberline {2.1.1.5}Iteraci\IeC {\'o}n 6}{8}
\contentsline {subsubsection}{\numberline {2.1.1.6}Iteraci\IeC {\'o}n 7}{9}
\contentsline {subsection}{\numberline {2.1.2}Diagrama de Gantt}{9}
\contentsline {subsection}{\numberline {2.1.3}Estimaci\IeC {\'o}n de costes}{11}
\contentsline {subsection}{\numberline {2.1.4}Evaluaci\IeC {\'o}n de riesgos}{13}
\contentsline {subsubsection}{\numberline {2.1.4.1}Riegos durante el desarrollo}{13}
\contentsline {subsubsection}{\numberline {2.1.4.2}Riesgos durante el uso de la aplicaci\IeC {\'o}n}{13}
\contentsline {chapter}{\numberline {3}An\IeC {\'a}lisis de Requisitos}{15}
\contentsline {section}{\numberline {3.1}Cat\IeC {\'a}logo de actores}{15}
\contentsline {subsection}{\numberline {3.1.1}Actores en la gu\IeC {\'\i }a virtual}{15}
\contentsline {subsection}{\numberline {3.1.2}Actores en panel de control}{15}
\contentsline {section}{\numberline {3.2}Requisitos funcionales}{16}
\contentsline {subsection}{\numberline {3.2.1}Gu\IeC {\'\i }a virtual}{16}
\contentsline {subsection}{\numberline {3.2.2}Panel de control}{16}
\contentsline {subsection}{\numberline {3.2.3}Servicio Web}{17}
\contentsline {section}{\numberline {3.3}Requisitos no funcionales}{17}
\contentsline {subsection}{\numberline {3.3.1}Gu\IeC {\'\i }a Virtual}{18}
\contentsline {subsection}{\numberline {3.3.2}Panel de control}{18}
\contentsline {subsection}{\numberline {3.3.3}Servicio web}{18}
\contentsline {section}{\numberline {3.4}Requisitos de informaci\IeC {\'o}n}{18}
\contentsline {section}{\numberline {3.5}Estudio de alternativas tecnol\IeC {\'o}gicas}{19}
\contentsline {subsection}{\numberline {3.5.1}Alternativas a Unity}{19}
\contentsline {subsubsection}{\numberline {3.5.1.1}CryENGINE}{19}
\contentsline {subsubsection}{\numberline {3.5.1.2}Unreal Engine}{19}
\contentsline {subsubsection}{\numberline {3.5.1.3}Shiva 3D}{20}
\contentsline {subsubsection}{\numberline {3.5.1.4}Torque 3D}{20}
\contentsline {chapter}{\numberline {4}An\IeC {\'a}lisis del Sistema}{21}
\contentsline {section}{\numberline {4.1}Diagrama de clases}{21}
\contentsline {subsection}{\numberline {4.1.1}Gu\IeC {\'\i }a virtual}{22}
\contentsline {subsection}{\numberline {4.1.2}Panel de control}{29}
\contentsline {subsection}{\numberline {4.1.3}Servicio Web}{31}
\contentsline {section}{\numberline {4.2}An\IeC {\'a}lisis de casos de uso}{32}
\contentsline {subsection}{\numberline {4.2.1}Gu\IeC {\'\i }a virtual}{33}
\contentsline {subsection}{\numberline {4.2.2}Panel de control}{35}
\contentsline {subsection}{\numberline {4.2.3}Servicio Web}{49}
\contentsline {section}{\numberline {4.3}Diagramas de secuencia}{50}
\contentsline {subsection}{\numberline {4.3.1}Gu\IeC {\'\i }a virtual}{50}
\contentsline {subsection}{\numberline {4.3.2}Panel de control}{52}
\contentsline {subsection}{\numberline {4.3.3}Servicio Web}{69}
\contentsline {chapter}{\numberline {5}Arquitectura del Sistema}{71}
\contentsline {section}{\numberline {5.1}Arquitectura f\IeC {\'\i }sica}{71}
\contentsline {section}{\numberline {5.2}Arquitectura l\IeC {\'o}gica}{72}
\contentsline {section}{\numberline {5.3}Dise\IeC {\~n}o f\IeC {\'\i }sico de datos}{73}
\contentsline {section}{\numberline {5.4}Interfaz de usuario}{77}
\contentsline {subsection}{\numberline {5.4.1}Interfaz de la gu\IeC {\'\i }a virtual}{77}
\contentsline {subsection}{\numberline {5.4.2}Interfaz del panel de control}{79}
\contentsline {chapter}{\numberline {6}Pruebas}{83}
\contentsline {section}{\numberline {6.1}Estrategia}{83}
\contentsline {section}{\numberline {6.2}Entorno de pruebas}{83}
\contentsline {section}{\numberline {6.3}Pruebas Unitarias}{83}
\contentsline {section}{\numberline {6.4}Pruebas de Integraci\IeC {\'o}n}{84}
\contentsline {section}{\numberline {6.5}Pruebas de Aceptaci\IeC {\'o}n}{84}
\contentsline {chapter}{\numberline {7}Conclusiones}{85}
\contentsline {section}{\numberline {7.1}Conclusiones sobre el trabajo realizado}{85}
\contentsline {section}{\numberline {7.2}Mejoras para el futuro}{86}
\contentsline {chapter}{Bibliograf\'{\i }a}{87}
\contentsline {chapter}{\numberline {A}Acr\IeC {\'o}nimos}{89}
\contentsline {chapter}{\numberline {B}Manual de implantaci\IeC {\'o}n}{91}
\contentsline {section}{\numberline {B.1}Requisitos}{91}
\contentsline {chapter}{\numberline {C}Manual de usuario}{95}
\contentsline {section}{\numberline {C.1}Manual de la Gu\IeC {\'\i }a Virtual}{95}
\contentsline {section}{\numberline {C.2}Manual del Panel de Control}{97}
